from tkinter import W
from django.shortcuts import render, redirect

# Create your views here.
from reviews.models import Review
from reviews.forms import ReviewForm


def list_reviews(request):
    reviews = Review.objects.all()
    context = {
        'reviews': reviews,
    }
    return render(request, 'reviews/main.html', context)


def create_review(request):
    # Create new form for this request
    form = ReviewForm()

    if request.method == 'POST':
        # Create a new form with the data in HTTP POST
        form = ReviewForm(request.POST)
        if form.is_valid():
            # It all looks good. Save the data w/this method
            form.save()
            # Redirect to the page which lists all the reviews after
            # successfully submitting form
            return redirect("reviews_list")

    # This won't run if both ^^ if statements are True
    # E.g. if request.method == GET then it will do this instead
    context = {
        'form': form,
    }
    # Render the template and return it to Django
    return render(request, "reviews/create.html", context)


def review_detail(request, id):
    review = Review.objects.get(id=id)
    context = {
        'review': review,
    }
    return render(request, "reviews/detail.html", context)
